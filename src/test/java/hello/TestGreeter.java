package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


// Starting class (3-4)
// rafael comment for assignment 14 
//Justin Jimenez A_14
// Grant Smith gcs37 Comment
// Jacob Jordan A_14
//Oscar Ramirez oar27 A_14

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

    @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 

   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }
   @Test
   @DisplayName("Test for Rafael")
   public void testGreeterRafael() 

   {
      g.setName("Rafael");
      assertEquals(g.getName(),"Rafael");
      assertEquals(g.sayHello(),"Hello Rafael!");
   }
   @Test
   @DisplayName("Rafael Test for Assert False assign 9")
   public void testGreeterFalseTester() 
   {
        int six = 6;
        int five = 5;
        g.setName("Rafael");
      assertFalse(g.getName()=="Rafaels Evil Twin");
      assertFalse((six+five)==12); 
      assertFalse(six<five); 
      
   }
   

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

   @Test
   @DisplayName("Test for Name='Justin'")
   public void testGreeterJustin() 
   {

      g.setName("Justin");
      assertEquals(g.getName(),"Justin");
      assertEquals(g.sayHello(),"Hello Justin!");
   }

   @Test
   @DisplayName("Test for Assert False")
   public void testGreeterFalse() 
   {

      g.setName("Justin");
      assertFalse(g.getName()=="Ted");
   }


   @Test
   @DisplayName("Test for Name='Jacob'")
   public void testGreeterJacob() 
   {

      g.setName("Jacob");
      assertEquals(g.getName(),"Jacob");
      assertEquals(g.sayHello(),"Hello Jacob!");
   }
   
   @Test
   @DisplayName("Test for Name='Nacho'")
   public void testGreeterNacho() 
   {

      g.setName("Nacho");
      assertFalse(g.getName()=="Jacob");
      assertFalse(g.sayHello()=="Hello Libre!");
	  assertFalse(g.sayHello()=="Goodbye Nacho!");
	  assertFalse(67 == 79);
	  assertFalse(45 == 54);
   }
   @Test
   @DisplayName("Test for Grant")
   public void testGreeterGrant() 

   {
      g.setName("Grant");
      assertEquals(g.getName(),"Grant");
      assertEquals(g.sayHello(),"Hello Grant!");
      assertFalse(false);
   }

   @Test
   @DisplayName("Test for Likes Bunnies")
   public void testLikesBunnies()
   {
       boolean likesBunnies = true;
       g.setName("Grant");
       String Name = g.getName();
       if (Name.equals("Grant") == true)
       {
           likesBunnies = false;
       }
       assertFalse(likesBunnies);

   }

   @Test
   @DisplayName("Test for Name='Oscar'")
   public void testGreeterOscar() 
   {

      g.setName("Oscar");
      assertEquals(g.getName(),"Oscar");
      assertEquals(g.sayHello(),"Hello Oscar!");
   }

   @Test
   @DisplayName("Test for Name='FalseAssert'")
   public void testGreeterFalseAssert() 
   {

      g.setName("False");
      assertFalse(g.getName()=="True");
      assertFalse(g.sayHello()=="Hello True!");
      assertFalse(3==2);
      assertFalse((45-12)==40);

      //Example of assertTrue for fun

      assertTrue(3!=2);
      assertTrue(50==50);


   }
   
   @Test
   @DisplayName("Test for Name='Jacob'")
   public void testGreeterJacobFalseAssert() 
   {
      g.setName("Jacob");
      assertFalse(g.getName()=="1234!@#");
      assertFalse(g.sayHello()=="Hello handsome!");
   }





   @Test
   @DisplayName("Test for Name='OscarTrueAssert'")
   public void testGreeterOscarTrueAssert() 
   {

      
      g.setName("True");
      assertTrue(g.getName()=="True");
      assertTrue(g.sayHello()!="Hello True!");

      
     

   }



   
}
